- +,-,0 floors

- a shapeshifting tool
    - you can negate floor, but only according to a random pattern
    - you can't upgrade, only reroll it

- story: Anubis testing Shiori
    - Shiori: overchagre -> - to +, + to 0
- roguelike again, but on logic base
    - a lot of potential help and risk
        - HELP
            - buttons
        - RISK
            - globally uniform tiles
            - switchable towers
            - one-way tiles
        - NEUTRAL
            - negating ghost
            - approaching enemies
                - only on +
                - only on -




- well, obviously, the new concept is strongly different
    - chaotic buttons that randomly negate the floors
    - current to-do
        - features
            - random disabler towers that turn off every fields in a range
            - GHOST DRONES!!!! who negate the floor in their path
            - spiderbots, who follow and attack you, but only on turned-on floors
        - technical
            - particles
            - credits
            - score
            - music & sounds
            - "start game" state
            - chasm tileset
            - procgun things




            1,1, 1, 1 -> floor to 0, bool to 0
            1,1, 0, 0 -> nothing
            1,2, 1, 0 -> bool to 1
            1,2, 1, 1 -> floor to 0, bool to 0

            1,0,0
            1,0,1
            0,1,0
            0,1,1



