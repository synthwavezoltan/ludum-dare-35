/**
 * Created by Katamori on 2016.04.16..
 */

var gameX = 800;
var gameY = 600;
var tilesize = 32;

//size of playing field
var fieldX = 64;
var fieldY = 64;

//size of player's sprite
var playerX = 64;
var playerY = 100;

var game = new Phaser.Game(gameX, gameY, Phaser.AUTO, '',
    { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.image('title', 'TITLE.png');
    game.load.image('tileset', 'TILESET.png');

    game.load.image('start', 'start.wav');
    game.load.image('step', 'step.wav');
    game.load.image('ghost', 'ghost.wav');
    game.load.image('negate', 'negate.wav');

    game.load.spritesheet('Shiori', 'GFX/SHIORI.png', 64, 100);
    game.load.spritesheet('button', 'GFX/BUTTON.png', tilesize, tilesize);
    game.load.spritesheet('ghost', 'GFX/GHOST.png', tilesize*2, tilesize*2);

    game.time.advancedTiming = true;

}

var field;
var layer;
var upper;
var player;
var a = 0;
var b = 0;
var buttons = [];
var ghosts = [];

var ghprob = 0;
var ghprobmax = 5;
var negation_trigger = [];

var welcome = true;

function create() {

    var start = game.add.audio('start');
    var step = game.add.audio('step');
    var ghost = game.add.audio('ghost');
    var negate = game.add.audio('negate');

    game.physics.startSystem(Phaser.Physics.ARCADE);

    drawable = game.add.group();

    //create tilemap as
    field = game.add.tilemap();

    field.addTilesetImage('tileset');

    //you CAN fly through
    field.setCollision([1, 5, 6, 13, 14, 15, 20, 22, 30]);

    //you CAN'T fly through
    field.setCollision(4);
    field.setCollision(12);



    layer = field.create('level', fieldX, fieldY, tilesize, tilesize);
    layer.resizeWorld();
    drawable.add(layer);

    upper = field.createBlankLayer('high_obstacles', fieldX, fieldY, tilesize, tilesize);
    upper.resizeWorld();
    drawable.add(upper);

    //upload tilemap
    for(i=0;i<field.width;i++){
        for(j=0;j<field.height;j++){


            if(i==0 || j==0 || i==field.width-1 || j==field.height-1){
                                var tile = 4;
            }else if(j<3) {     var tile = 12;
            } else {            var tile = 0;
            };
            //var tile = Math.floor((Math.random() * 3));


            if(tile == 0){field.putTile(tile, i, j, layer)}else{field.putTile(tile, i, j, upper)};
            //field.putTile(8, i, j, upper);
            //print(tile, 10, i*tilesize, j*tilesize);
        };
    };

    //randomize tilemap
    Randomize_Tilemap();

/*
    for(i=0;i<field.width;i++){
        for(j=0;j<field.height;j++){

            if (field.getTile(i, j, upper) != null){
                print(field.getTile(i, j, upper).index, 10, i*tilesize, j*tilesize);
            };
        };
    };
*/



    //define player
    Create_Player('Shiori', 32, 96-playerX);

    cursors = game.input.keyboard.createCursorKeys();

    drawable.sort();

    titlepic = game.add.sprite(0,0, 'title');
    game.physics.arcade.enable(titlepic);

    titlepic.inputEnabled = true;
    titlepic.input.useHandCursor = true;
    titlepic.events.onInputDown.add(destroySprite, this);
}

function update() {

    if (!titlepic.visible) {

        ghprob = Math.floor((Math.random() * 1000));
        if ( ghprob < ghprobmax ){ Create_Ghost() };

        Collision_Enabler();

        player.body.velocity.x = 0;
        player.body.velocity.y = 0;

        if (cursors.up.isDown) {        player.body.velocity.y = -200};
        if (cursors.left.isDown) {      player.body.velocity.x = -200};
        if (cursors.right.isDown) {     player.body.velocity.x = 200};
        if (cursors.down.isDown) {      player.body.velocity.y = 200};

        if (cursors.left.isDown) {         player.animations.play('run left')}
        else if (cursors.right.isDown ||
            cursors.up.isDown ||
            cursors.down.isDown) {         player.animations.play('run right')}
        else{                                   player.animations.stop(); player.frame = 0};

    };

}

function render() {

    //game.debug.body(player);
    //game.debug.bodyInfo(player,0,90);

    //game.debug.text("FPS: "+game.time.fps, gameX - 80, 64);
    //game.debug.text(ghprob +"|"+ghosts.length, gameX - 80, 128);

    //game.debug.text(titlepic.visible + welcome, 0,396);

    //game.debug.text(field.layers[0].name + "|" + field.layers[1].name,0,64);

}





/*

CUSTOM MADE FUNCTIONS

 */


function print(string, size, x, y) {

    var text = string;
    var style = {font: size+"px Arial", fill: "white", align: "left"};

    var t = game.add.text(x, y, text, style);

};

function Create_Player(sheet, x, y){

    player = game.add.sprite(x, y, sheet);
    drawable.add(player);

    player.animations.add('run right', [1,2], 3, true, true);
    player.animations.add('run left', [4, 5], 3, true, true);

    game.physics.arcade.enable(player);

    player.body.bounce.y = 0.2;
    player.body.collideWorldBounds = true;
    player.body.setSize(tilesize, tilesize, (playerX-tilesize)/2, playerY-tilesize);


    game.camera.follow(player);

};

//dir: 0-1-2-3 ONLY!
function Create_Button(x, y, dir){


    var d = dir*4
    buttons.push(game.add.sprite(x, y, 'button', d));
    var newid = buttons.length-1;

    buttons[newid].animations.add('base', [d,d+1,d+2,d+3], 2, true, true);
    buttons[newid].animations.play('base');

    drawable.add(buttons[newid]);

    game.physics.arcade.enable(buttons[newid]);
    buttons[newid].body.immovable = true;


};

function Create_Ghost(){

        do {
            a = Math.floor((Math.random() * (field.width - 5)) + 5);
            b = Math.floor((Math.random() * (field.height - 5)) + 5);
        }while(field.getTile(a, b, layer) == null || field.getTile(a, b, layer).index != 24);

        ghosts.push(game.add.sprite(a*tilesize, b*tilesize, 'ghost'));
        var newid = ghosts.length -1;

        //buttons[newid].animations.add('base', [d,d+1,d+2,d+3], 2, true, true);
        //buttons[newid].animations.play('base');

        ghosts[newid].alpha = 0.6;

        drawable.add(ghosts[newid]);

        game.physics.arcade.enable(ghosts[newid]);
        ghosts[newid].body.bounce.x = 1;
        ghosts[newid].body.bounce.y = 1;
        ghosts[newid].body.velocity.x = Math.floor((Math.random() * 300) - 150);
        ghosts[newid].body.velocity.y = Math.floor((Math.random() * 300) - 150);

        //negation trigger
        negation_trigger.push(true);


};


function Collision_Enabler(){

    game.physics.arcade.collide(player, layer);
    game.physics.arcade.collide(player, upper);

    for(i=0;i<buttons.length;i++){
        game.physics.arcade.collide(player, buttons[i], Button_Behaviour, Button_Callback, this);
        //game.physics.arcade.collide(buttons[i], layer);
    };

    for(i=0;i<ghosts.length;i++){
        //game.physics.arcade.collide(player, buttons[i], Button_Behaviour, Button_Callback, this);
        game.physics.arcade.collide(ghosts[i], upper);

        Ghost_Behaviour(i);
    };

};

function Button_Callback(){ return true};


function Button_Behaviour(obj1, obj2){

    var ownx = layer.getTileX(obj2.body.x);
    var owny = layer.getTileX(obj2.body.y);

    if(obj2.frame < 4){                                 Random_Negation(1, 1, field.width-1, owny-1);
    } else if (obj2.frame >= 4 && obj2.frame < 8){      Random_Negation(ownx+1, 1, field.width-1, field.height-1);
    } else if (obj2.frame >= 8 && obj2.frame < 12) {    Random_Negation(1, owny+1, field.width-1, field.height-1);
    } else if (obj2.frame >= 12 && obj2.frame < 16) {   Random_Negation(1, 1, ownx-1, field.height-1);
    };

};

function Random_Negation(thrx_a, thry_a, thrx_b, thry_b) {

    do {
        a = Math.floor((Math.random() * thrx_b - 1) + thrx_a);
        b = Math.floor((Math.random() * thry_b - 1) + thry_a);
    }while(field.getTile(a, b, layer) == null || field.getTile(a, b, layer).index > 1);

    //Negation(ownx,owny);
    if(field.getTile(a, b, layer) != null && field.getTile(a, b, layer).index == 0){
        field.putTile(1, a, b, layer);
    } else
    if(field.getTile(a, b, layer) != null && field.getTile(a, b, layer).index ==1){
        field.putTile(0, a, b, layer);
    };


};

function Ghost_Behaviour(id){

    var ownx = layer.getTileX(ghosts[id].body.x);
    var owny = layer.getTileY(ghosts[id].body.y);

    var prex = layer.getTileX(ghosts[id].body.x - (ghosts[id].body.velocity.x/10));
    var prey = layer.getTileY(ghosts[id].body.y - (ghosts[id].body.velocity.y/10));

    if(field.getTile(ownx, owny, layer) != null && field.getTile(ownx, owny, layer).index < 2 &&
        field.getTile(prex, prey, layer) != null){
        if(ownx == prex && owny == prey){


            if( negation_trigger[id] == true){

                var newv = field.getTile(ownx, owny, layer).index;

                field.putTile(1-newv, ownx, owny, layer);

                negation_trigger[id] = false;
            }

        }else{negation_trigger[id] = true;};
    }

};

function destroySprite (sprite) {

    sprite.visible = false;

}

function Randomize_Tilemap(){


    for (m=1; m<100; m++){

        //turned-off tiles
        do {
            a = Math.floor((Math.random() * (field.width - 5)) + 5);
            b = Math.floor((Math.random() * (field.height - 5)) + 5);
        }while(field.getTile(a, b, layer) == null || field.getTile(a, b, layer).index != 0);

        field.putTile(1, a, b, layer);
    };

    for (m=1; m<500; m++){

        //neutral floors
        do {
            a = Math.floor((Math.random() * (field.width - 5)) + 5);
            b = Math.floor((Math.random() * (field.height - 5)) + 5);
        }while(field.getTile(a, b, layer) == null || field.getTile(a, b, layer).index > 1);

        field.putTile(Math.floor((Math.random() * 2))+2, a, b, layer);
    };



    //runes
    for(m=1;m<30;m++){


        do {
            a = Math.floor((Math.random() * (field.width - 5)) + 5);
            b = Math.floor((Math.random() * (field.height - 5)) + 5);
        }while(field.getTile(a, b, layer) == null || field.getTile(a, b, layer).index > 1);

        var putthat = Math.floor((Math.random() * 5));
        field.putTile(putthat + 24, a, b, layer);
    };

    //chasm
    for(m=1;m<15;m++){


        do {
            a = Math.floor((Math.random() * (field.width - 10)) + 10);
            b = Math.floor((Math.random() * (field.height - 10)) + 10);
        }while(field.getTile(a, b, layer) == null
        || field.getTile(a, b, layer).index == 2);


        var temp = Math.floor((Math.random() * 5) + 1);

        for(p=0;p<temp;p++){

            for(t=1; t<4; t++ ){

                field.putTile(30-(t*8), a+p, b-t, layer);

            }

        };


        field.putTile(30, a, b, layer);
        field.putTile(30, a+1, b, layer);
        field.putTile(30, a+1, b+1, layer);
        field.putTile(30, a, b+1, layer);
        field.putTile(30, a+1, b+2, layer);
        field.putTile(30, a, b+2, layer);

        field.putTile(3, a, b-4, layer);
        field.putTile(3, a+1, b-4, layer);

    };

    //additional cell-automaton-like function
    for(i=5;i<field.width-5;i++){
        for(j=5;j<field.height-5;j++){


            if(field.getTile(i, j, layer) != null){

                //more neutrual tiles
                 if( field.getTile(i, j, layer).index == 2 ||
                     field.getTile(i, j, layer).index == 3){
                     field.putTile(3, i, j-1, layer);
                 };

                //chasm edges
                if( field.getTile(i, j, layer).index == 30 &&
                    field.getTile(i, j+1, layer).index != 30){
                    field.putTile(20, i, j+1, layer);
                };

                if( field.getTile(i, j, layer).index%8 == 6){
                    if( field.getTile(i+1, j, layer).index%8 != 6){ field.putTile(20, i+1, j, layer)};
                    if( field.getTile(i-1, j, layer).index%8 != 6){ field.putTile(20, i-1, j, layer)};


                }

            };

        };
    };

    //buttons
    for (m=1; m<20; m++){

        do {
            a = Math.floor((Math.random() * (field.width - 5)) + 5);
            b = Math.floor((Math.random() * (field.height - 5)) + 5);
        }while(field.getTile(a, b, layer) == null || field.getTile(a, b, layer).index > 1);

        field.putTile(3, a, b, layer);
        Create_Button(a*tilesize, b*tilesize, Math.floor(Math.random() * 90)%4);
    };

}